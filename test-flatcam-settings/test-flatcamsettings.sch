EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "4 aug 2015"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L CONN_7 P1
U 1 1 55C0BB1D
P 6900 2300
F 0 "P1" V 6870 2300 60  0000 C CNN
F 1 "CONN_7" V 6970 2300 60  0000 C CNN
F 2 "~" H 6900 2300 60  0000 C CNN
F 3 "~" H 6900 2300 60  0000 C CNN
	1    6900 2300
	1    0    0    -1  
$EndComp
$Comp
L CONN_7 P2
U 1 1 55C0BB34
P 6900 3200
F 0 "P2" V 6870 3200 60  0000 C CNN
F 1 "CONN_7" V 6970 3200 60  0000 C CNN
F 2 "~" H 6900 3200 60  0000 C CNN
F 3 "~" H 6900 3200 60  0000 C CNN
	1    6900 3200
	1    0    0    -1  
$EndComp
$Comp
L CONN_7 P3
U 1 1 55C0BB3A
P 6900 4100
F 0 "P3" V 6870 4100 60  0000 C CNN
F 1 "CONN_7" V 6970 4100 60  0000 C CNN
F 2 "~" H 6900 4100 60  0000 C CNN
F 3 "~" H 6900 4100 60  0000 C CNN
	1    6900 4100
	1    0    0    -1  
$EndComp
$Comp
L CONN_7 P4
U 1 1 55C0BB40
P 6900 5000
F 0 "P4" V 6870 5000 60  0000 C CNN
F 1 "CONN_7" V 6970 5000 60  0000 C CNN
F 2 "~" H 6900 5000 60  0000 C CNN
F 3 "~" H 6900 5000 60  0000 C CNN
	1    6900 5000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6550 2600 6300 2600
Wire Wire Line
	6300 2600 6300 5300
Wire Wire Line
	6300 5300 6550 5300
Wire Wire Line
	6550 3500 6300 3500
Connection ~ 6300 3500
Wire Wire Line
	6550 4400 6300 4400
Connection ~ 6300 4400
Wire Wire Line
	6550 2500 6250 2500
Wire Wire Line
	6250 2500 6250 5200
Wire Wire Line
	6250 5200 6550 5200
Wire Wire Line
	6550 4300 6250 4300
Connection ~ 6250 4300
Wire Wire Line
	6550 3400 6250 3400
Connection ~ 6250 3400
Wire Wire Line
	6550 2400 6200 2400
Wire Wire Line
	6200 2400 6200 5100
Wire Wire Line
	6200 5100 6550 5100
Wire Wire Line
	6550 2300 6150 2300
Wire Wire Line
	6150 2300 6150 5000
Wire Wire Line
	6150 5000 6550 5000
Wire Wire Line
	6550 2200 6100 2200
Wire Wire Line
	6100 2200 6100 4900
Wire Wire Line
	6100 4900 6550 4900
Wire Wire Line
	6550 2100 6050 2100
Wire Wire Line
	6050 2100 6050 4800
Wire Wire Line
	6050 4800 6550 4800
Wire Wire Line
	6550 2000 6000 2000
Wire Wire Line
	6000 2000 6000 4700
Wire Wire Line
	6000 4700 6550 4700
Wire Wire Line
	6550 3300 6200 3300
Connection ~ 6200 3300
Wire Wire Line
	6550 3200 6150 3200
Connection ~ 6150 3200
Wire Wire Line
	6550 3100 6100 3100
Connection ~ 6100 3100
Wire Wire Line
	6550 3000 6050 3000
Connection ~ 6050 3000
Wire Wire Line
	6550 2900 6000 2900
Connection ~ 6000 2900
Wire Wire Line
	6550 4200 6200 4200
Connection ~ 6200 4200
Wire Wire Line
	6550 4100 6150 4100
Connection ~ 6150 4100
Wire Wire Line
	6550 4000 6100 4000
Connection ~ 6100 4000
Wire Wire Line
	6550 3900 6050 3900
Connection ~ 6050 3900
Wire Wire Line
	6550 3800 6000 3800
Connection ~ 6000 3800
$EndSCHEMATC
